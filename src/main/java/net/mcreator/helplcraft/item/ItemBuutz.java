
package net.mcreator.helplcraft.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.world.World;
import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.helplcraft.procedure.ProcedureBuutzBootsTickEvent;
import net.mcreator.helplcraft.ElementsHelplcraft;

@ElementsHelplcraft.ModElement.Tag
public class ItemBuutz extends ElementsHelplcraft.ModElement {
	@GameRegistry.ObjectHolder("helplcraft:buutzhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("helplcraft:buutzbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("helplcraft:buutzlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("helplcraft:buutzboots")
	public static final Item boots = null;
	public ItemBuutz(ElementsHelplcraft instance) {
		super(instance, 40);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("BUUTZ", "helplcraft:helpliumarmor", 25, new int[]{1, 5, 6, 2}, 10,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET) {
			@Override
			public void onArmorTick(World world, EntityPlayer entity, ItemStack itemstack) {
				int x = (int) entity.posX;
				int y = (int) entity.posY;
				int z = (int) entity.posZ;
				{
					java.util.HashMap<String, Object> $_dependencies = new java.util.HashMap<>();
					$_dependencies.put("entity", entity);
					ProcedureBuutzBootsTickEvent.executeProcedure($_dependencies);
				}
			}
		}.setUnlocalizedName("buutzboots").setRegistryName("buutzboots").setCreativeTab(CreativeTabs.COMBAT));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("helplcraft:buutzboots", "inventory"));
	}
}
