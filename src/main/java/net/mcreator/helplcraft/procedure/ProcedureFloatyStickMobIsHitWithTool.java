package net.mcreator.helplcraft.procedure;

import net.minecraft.potion.PotionEffect;
import net.minecraft.init.MobEffects;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.Entity;

import net.mcreator.helplcraft.ElementsHelplcraft;

@ElementsHelplcraft.ModElement.Tag
public class ProcedureFloatyStickMobIsHitWithTool extends ElementsHelplcraft.ModElement {
	public ProcedureFloatyStickMobIsHitWithTool(ElementsHelplcraft instance) {
		super(instance, 14);
	}

	public static void executeProcedure(java.util.HashMap<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure FloatyStickMobIsHitWithTool!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof EntityLivingBase)
			((EntityLivingBase) entity).addPotionEffect(new PotionEffect(MobEffects.LEVITATION, (int) 15, (int) 7));
	}
}
